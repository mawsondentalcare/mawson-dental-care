Mawson Dental Care is committed to the provision of the highest quality dental care in Canberra. Our services focus on preventive dental care. Other treatment offered includes but is not limited to, crown and bridgework, implants, root canal treatment, extractions and tooth whitening.

Address: 3/142-152 Mawson Place, Mawson, ACT 2607, Australia

Phone: +61 2 6290 0055

Website: https://www.mawsondentalcare.com.au
